//
//  ViewController.swift
//  Egypt Landmarks
//
//  Created by amr on 3/14/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class landMarksListVC: UIViewController , UITableViewDelegate, UITableViewDataSource {
    //MARK: - CONSTANTS
    let cellID = "landmarkCell"
    let detailsSeque = "toDetails"
    
    @IBOutlet weak var landmarksTV: UITableView!
    
    let landmarks : [Landmark] = [Landmark(name: "El-Karnak", city: "Luxor, Egypt" , image: "karnak", discription: "The Karnak Temple Complex, commonly known as Karnak, comprises a vast mix of decayed temples, chapels, pylons, and other buildings in Egypt", lat: "25.7030771", long: "32.66730180000002"),
                                  Landmark(name: "Cairo Citadel", city: "Cairo, Egypt" , image: "citadel", discription: "The Saladin Citadel of Cairo is a medieval Islamic fortification in Cairo, Egypt. The location, on Mokattam hill near the center of Cairo, was once famous for its fresh breeze and grand views of the city", lat: "30.0216667", long: "31.303333299999963"),
                                  Landmark(name: "Mount Sinai", city: "Sinai, Egypt" , image: "mountSinai", discription: "Mount Sinai, also known as Mount Horeb or Gabal Musa, is a mountain in the Sinai Peninsula of Egypt that is a possible location of the biblical Mount Sinai, which is considered a holy site by the Abrahamic religions.", lat: "29.5", long: "34"),
                                  Landmark(name: "Egyptian Museum", city: "Cairo, Egypt" , image: "museum", discription: "The Museum of Egyptian Antiquities, known commonly as the Egyptian Museum or Museum of Cairo, in Cairo, Egypt, is home to an extensive collection of ancient Egyptian antiquities", lat: "30.0478468", long: "31.233649300000025")]
    override func viewDidLoad() {
        super.viewDidLoad()
        landmarksTV.delegate = self
        landmarksTV.dataSource = self
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    //MARK:- TableView Functions

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return landmarks.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! landmarkCell
        cell.cityLabel.text = landmarks[indexPath.row].city
        cell.nameLabel.text = landmarks[indexPath.row].name
        cell.landmarkImage.image = UIImage(named: landmarks[indexPath.row].image)
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: detailsSeque, sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == detailsSeque {
           let vc = segue.destination as! DetailVC
            if let indexPath = self.landmarksTV.indexPathForSelectedRow {
                
            vc.landmark = landmarks[indexPath.row]
            }
        }
    }
   


}

