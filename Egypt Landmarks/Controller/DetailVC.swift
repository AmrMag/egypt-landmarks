//
//  DetailVC.swift
//  Egypt Landmarks
//
//  Created by amr on 3/14/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit
import MapKit
class DetailVC: UIViewController {

    var landmark : Landmark!
    
    var lat : Double = 0.0
    var long : Double = 0.0
    @IBOutlet weak var landmarkMap: MKMapView!
    @IBOutlet weak var landmarkTextView: UITextView!
    @IBOutlet weak var landmarkCityLabel: UILabel!
    @IBOutlet weak var landmarkNameLabel: UILabel!
    @IBOutlet weak var landmarkImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

       self.navigationItem.title = landmark?.name
        if let lat = Double(landmark.lat), let long = Double(landmark.long) {
            self.lat = lat
            self.long = long
            
        }
        print("lat is\(self.lat) long is\(self.long)")
        updateMap(lat: self.lat, long: self.long)
        updateUI()
    }
    func updateMap(lat : Double,  long : Double){
        let span = MKCoordinateSpanMake(0.005, 0.005)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2DMake(lat, long), span: span)
        landmarkMap.setRegion(region, animated: true)
        let pinLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, long)
        let pinAnn = MKPointAnnotation()
        pinAnn.coordinate = pinLocation
        pinAnn.title = landmark.name
        pinAnn.subtitle = landmark.city
        self.landmarkMap.addAnnotation(pinAnn)
    }
    func updateUI(){
        landmarkNameLabel.text = landmark.name
        landmarkCityLabel.text = landmark.city
        landmarkImage.image = UIImage(named: landmark.image)
        landmarkTextView.text = landmark.discription
    }
    

    
    @IBAction func ditectionsClicked(_ sender: Any) {
        UIApplication.shared.open(URL(string: "http://maps.apple.com/maps?daddr=\(self.lat),\(self.long)")!, options: [:], completionHandler: nil)
    }
    
}
