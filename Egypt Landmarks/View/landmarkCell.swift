//
//  landmarkCellTableViewCell.swift
//  Egypt Landmarks
//
//  Created by amr on 3/14/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class landmarkCell: UITableViewCell {

    @IBOutlet weak var landmarkImage: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
