//
//  Landmark.swift
//  Egypt Landmarks
//
//  Created by amr on 3/14/18.
//  Copyright © 2018 amr. All rights reserved.
//

import Foundation
class Landmark {
    let name : String
    let city : String
    let image : String
    let discription : String
    let lat : String
    let long : String
    
    init(name : String, city: String, image: String , discription : String, lat :String,long : String) {
        self.name = name
        self.city = city
        self.image = image
        self.discription = discription
        self.lat = lat
        self.long = long
    }
}
